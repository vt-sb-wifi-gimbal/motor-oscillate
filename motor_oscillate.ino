int num = 0;
char cmotor = 'a'; //default is a
unsigned long now;
const float pi = 3.1415926;

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
}

void loop() {
  if (Serial.available()) {
    while (1) {
      char in = Serial.read();
      if (in == 'a' || in == 'b') {
        cmotor = in;
        break;
      }
      if (in == -1) continue; //if no char in buffer
      if (in < 58 && in > 47) { //0:48,1:49,2:50,3:51,4:52,5:53,6:54,7:55,8:56,9:57
        //read chars for speed
      }
      if (in == ';') {
        halt();
      }
    }
  }

  //run motors
  byte s = 145;
  byte variation = 0;
  yaw(s);
  float delta = sin(pi * millis() / 1000.0);
  byte p = s + ((delta * variation));
  p = s+10;
  pitch(p);
  Serial.print(s);
  Serial.print(' ');
  Serial.println(p);
}



//Helper functions
void yaw(byte v) {
  Serial2.write(v);
}
void pitch(byte v) {
  Serial1.write(v);
}
void both(byte v) {
  yaw(v);
  pitch(v);
}
void halt() {
  both(127);
  Serial.println("DONE");
}
